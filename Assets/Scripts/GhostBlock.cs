﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostBlock : MonoBehaviour
{
    GameObject parent;
    TetrisBlock parentBlock;
    // Start is called before the first frame update
    void Start()
    {
        //use coroutine in order to only calculate every 0.1s and not every update call to save perfomance.
        StartCoroutine(RepositionBlock());
    }

    public void SetParent(GameObject inParent)
    {
        parent = inParent;
        parentBlock = parent.GetComponent<TetrisBlock>();
    }

    void PositionGhost()
    {
        transform.position = parent.transform.position;
        transform.rotation = parent.transform.rotation;
    }

    void MoveDown()
    {
        while (IsValidMove())
        {
            transform.position += Vector3.down;
        }

        //after the while loop is over we went 1 space down too much
        transform.position += Vector3.up;
    }

    bool IsValidMove()
    {
        foreach (Transform child in transform)
        {
            Vector3 pos = Playfield.instance.Round(child.position);
            if (!Playfield.instance.CheckInsidePlayField(pos))
            {
                return false;
            }
        }

        foreach (Transform child in transform)
        {
            Vector3 pos = Playfield.instance.Round(child.position);
            Transform t = Playfield.instance.GetTransformOnGridPos(pos);

            if (t!=null && t.parent == parent.transform)
            {
                return true;
            }
            //Position already taken
            if (t != null && t.parent != transform)
            {
                return false;
            }
        }

        return true;
    }

    IEnumerator RepositionBlock()
    {
        while (parentBlock.enabled)
        {
            PositionGhost();
            //Move the ghost all the way down down
            MoveDown();
            yield return new WaitForSeconds(0.1f);
        }
        Destroy(gameObject);
        yield return null;
    }
}
