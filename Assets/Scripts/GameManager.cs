﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    
    int score;
    int level;
    int layersCleared;

    bool gameIsOver;
    float fallSpeed;

    
    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        UpdateScore(score);
    }

    public float GetFallSpeed()
    {
        return fallSpeed;
    }

    public bool IsGameOver()
    {
        return gameIsOver;
    }

    public void GameIsOver()
    {
        gameIsOver = true;
        UIHandler.instance.ActivateGameOver();
    }

    public void UpdateScore(int amount)
    {
        score += amount;
        CalculateLevel();
        UIHandler.instance.UpdateUI(score,level,layersCleared);
    }

    public void LayerCleared(int amount)
    {
        UpdateScore(amount * 400);
        layersCleared += amount;
        UIHandler.instance.UpdateUI(score, level, layersCleared);
    }

    void CalculateLevel()
    {
        level = (int)score / 5000;
        level++;
        if (level > 8)
        {
            level = 8;
        }
        fallSpeed = 3.25f - (level * 0.25f);

        UIHandler.instance.UpdateUI(score, level, layersCleared);
    }
}
