﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts
{
    public static class Extensions
    {
        public static Vector3 FromString(this Vector3 vector, string value)
        {
            string[] temp = value.Replace(" ", "").Split(',');
            vector.x = float.Parse(temp[0]);
            vector.y = float.Parse(temp[1]);
            vector.z = float.Parse(temp[2]);

            return vector;
        }

        public static Vector3 ToVector3(this string value)
        {
            Vector3 vector = new Vector3();
            string[] temp = value.Replace(" ", "").Split(',');
            vector.x = float.Parse(temp[0]);
            vector.y = float.Parse(temp[1]);
            vector.z = float.Parse(temp[2]);

            return vector;
        }

        public static Color ToColor(this string color)
        {
            return (Color)typeof(Color).GetProperty(color.ToLowerInvariant()).GetValue(null, null);
        }
    }
}