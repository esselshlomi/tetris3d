﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHandler : MonoBehaviour
{
    public static UIHandler instance;

    public Text scoreText;
    public Text levelText;
    public Text layersText;

    public GameObject gameOverWindow;

    private void Start()
    {
        gameOverWindow.SetActive(false);
    }

    public void ActivateGameOver()
    {
        gameOverWindow.SetActive(true);
    }

    private void Awake()
    {
        instance = this;
    }

    public void UpdateUI(int score,int level, int layersCleared)
    {
        scoreText.text = $"Score: {score.ToString("D9")}";
        levelText.text = $"Level: {level}";
        layersText.text = $"Layers: {layersCleared.ToString("D9")}";
    }
}
