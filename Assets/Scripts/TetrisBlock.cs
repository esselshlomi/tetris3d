﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TetrisBlock : MonoBehaviour
{
    float prevTime;
    float fallTime = 1f;

    Dictionary<string, Vector3> keyToDirection;
    
    // Start is called before the first frame update
    void Start()
    {
        fallTime = GameManager.instance.GetFallSpeed();
        if (!CheckValidMove())
        {
            GameManager.instance.GameIsOver();
        }
        keyToDirection = new Dictionary<string, Vector3>();
        keyToDirection.Add("left", Vector3.left);
        keyToDirection.Add("right", Vector3.right);
        keyToDirection.Add("back", Vector3.back);
        keyToDirection.Add("forward", Vector3.forward);
    }

    private Vector3 GetAKeyVector()
    {
        Camera camera = Camera.main;
        Vector3 camPos = camera.transform.position;
        var distToNorth = Vector3.Distance(Playfield.instance.North.transform.position, camPos);
        var distToSouth = Vector3.Distance(Playfield.instance.South.transform.position, camPos);
        var distToEast = Vector3.Distance(Playfield.instance.East.transform.position, camPos);
        var distToWest = Vector3.Distance(Playfield.instance.West.transform.position, camPos);
        float closestWallDist = Mathf.Min(distToWest, distToEast, distToNorth, distToSouth);
        if (closestWallDist == distToSouth)
        {
            return(keyToDirection["left"]);
        }
        else if (closestWallDist == distToNorth)
        {
            return (keyToDirection["right"]);
        }
        else if (closestWallDist == distToEast)
        {
            return (keyToDirection["back"]);
        }
        else
        {
            return (keyToDirection["forward"]);
        }
    }
    Vector3 GetWKeyVector()
    {
        Camera camera = Camera.main;
        Vector3 camPos = camera.transform.position;
        var distToNorth = Vector3.Distance(Playfield.instance.North.transform.position, camPos);
        var distToSouth = Vector3.Distance(Playfield.instance.South.transform.position, camPos);
        var distToEast = Vector3.Distance(Playfield.instance.East.transform.position, camPos);
        var distToWest = Vector3.Distance(Playfield.instance.West.transform.position, camPos);
        float closestWallDist = Mathf.Min(distToWest, distToEast, distToNorth, distToSouth);
        if (closestWallDist == distToSouth)
        {
            return (keyToDirection["forward"]);
        }
        else if (closestWallDist == distToNorth)
        {
            return (keyToDirection["back"]);
        }
        else if (closestWallDist == distToEast)
        {
            return (keyToDirection["left"]);
        }
        else
        {
            return (keyToDirection["right"]);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - prevTime > fallTime)
        {
            transform.position += Vector3.down;
            if (!CheckValidMove())
            {
                transform.position += Vector3.up;
                //Delete layer if need 
                Playfield.instance.DeleteLayer();
                enabled = false;
                //Create new block
                if (!GameManager.instance.IsGameOver())
                {
                    Playfield.instance.PickPatternAndSpawnBlock();
                }
            }
            else
            {
                //Update the grid
                Playfield.instance.UpdateGrid(this);
            }
            prevTime = Time.time;
        }
        
        if (Input.GetKeyDown(KeyCode.A))
        {
            Vector3 moveVector = GetAKeyVector();
            SetMovementInput(moveVector);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            Vector3 moveVector = GetAKeyVector();
            moveVector = moveVector * -1;
            SetMovementInput(moveVector);
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            Vector3 moveVector = GetWKeyVector();
            SetMovementInput(moveVector);
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            Vector3 moveVector = GetWKeyVector();
            moveVector = moveVector * -1;
            SetMovementInput(moveVector);
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            SetRotationInput(new Vector3(90, 0, 0));
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            SetRotationInput(new Vector3(0, 90, 0));
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            SetRotationInput(new Vector3(0, 0, 90));
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            SetSpeed(0.1f);
        }
    }

    private void SetMovementInput(Vector3 direction)
    {
        transform.position += direction;
        if (!CheckValidMove())
        {
            transform.position -= direction;
        }
        else
        {
            Playfield.instance.UpdateGrid(this);
        }
    }

    private void SetRotationInput(Vector3 rotation)
    {
        transform.Rotate(rotation, Space.World);
        if (!CheckValidMove())
        {
            transform.Rotate(-rotation, Space.World);
        }
        else
        {
            Playfield.instance.UpdateGrid(this);
        }
    }

    bool CheckValidMove()
    {
        foreach(Transform child in transform)
        {
            Vector3 pos = Playfield.instance.Round(child.position);
            if (!Playfield.instance.CheckInsidePlayField(pos))
            {
                return false;
            }
        }
        foreach(Transform child in transform)
        {
            Vector3 pos = Playfield.instance.Round(child.position);
            Transform t = Playfield.instance.GetTransformOnGridPos(pos);
            //Position already taken
            if (t != null && t.parent != transform)
            {
                return false;
            }
        }

        return true;
    }

    public void SetSpeed(float speed)
    {
        fallTime = speed;
    }
}
