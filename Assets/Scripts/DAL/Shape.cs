﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Assets.Scripts
{
	[XmlRoot(ElementName = "shape")]
	public class Shape
	{
		[XmlElement(ElementName = "title")]
		public string Title { get; set; }

		[XmlElement(ElementName = "SquarePosition")]
		public List<string> SquarePosition { get; set; }

		[XmlElement(ElementName = "Weight")]
		public string Weight { get; set; }

		[XmlElement(ElementName = "color")]
		public string Color { get; set; }
	}
}