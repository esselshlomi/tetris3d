﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;

namespace Assets.Scripts
{
	[XmlRoot(ElementName = "shapes")]
	public class Shapes
	{
		[XmlElement(ElementName = "shape")]
		public List<Shape> Shape { get; set; }

        public static Shapes Load(string path)
        {
            var serializer = new XmlSerializer(typeof(Shapes));
            using (var stream = new FileStream(path, FileMode.Open))
            {
                return serializer.Deserialize(stream) as Shapes;
            }
        }
    }
}