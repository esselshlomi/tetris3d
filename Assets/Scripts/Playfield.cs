﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Playfield : MonoBehaviour
{
    public static Playfield instance;

    public int GridSizeX;
    public int GridSizeY;
    public int GridSizeZ;

    private List<BlockPattern> BlocksPatterns;

    [Header("Playfield Visuals")]
    public GameObject bottomGrid;
    public GameObject North;
    public GameObject South;
    public GameObject West;
    public GameObject East;

    public Transform[,,] theGrid;

    private int _nextIndex;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        theGrid = new Transform[GridSizeX, GridSizeY, GridSizeZ];
        LoadPatterns();
        CalculatePreview();
        PickPatternAndSpawnBlock();
    }

    private void LoadPatterns()
    {
        BlocksPatterns = new List<BlockPattern>();
        //Load from file
        var allShapes = Shapes.Load(Path.Combine(Application.dataPath, "shapes.xml"));
        //Create pattern from shape
        foreach(Shape currentShape in allShapes.Shape)
        {
            List<Vector3> blockPositions = new List<Vector3>();
            foreach (var blockPosition in currentShape.SquarePosition)
            {
                blockPositions.Add(blockPosition.ToVector3());
            }
            Color shapeColor = currentShape.Color.ToColor();
            BlockPattern block = new BlockPattern()
            {
                BlocksPositions = blockPositions,
                Color = shapeColor
            };
            //To increase block weight we add it multiple times to increase chance it get picked
            for (int i = 0; i < int.Parse(currentShape.Weight); i++)
            {
                BlocksPatterns.Add(block);
            }
        }
    }

    public void UpdateGrid(TetrisBlock block)
    {
        //Delete possible parent objects
        for (int x = 0; x < GridSizeX; x++)
        {
            for (int z = 0; z < GridSizeZ; z++)
            {
                for (int y = 0; y < GridSizeY; y++)
                {
                    if (theGrid[x, y, z] != null)
                    {
                        if (theGrid[x, y, z].parent == block.transform)
                        {
                            theGrid[x, y, z] = null;
                        }
                    }
                }
            }
        }
        
        //Fill all the child blocks objects
        foreach(Transform childBlock in block.transform)
        {
            Vector3 pos = Round(childBlock.position);
            if (pos.y < GridSizeY)
            {
                theGrid[(int)pos.x, (int)pos.y, (int)pos.z] = childBlock;
            }
        }
    }

    public Transform GetTransformOnGridPos(Vector3 pos)
    {
        if (pos.y > GridSizeY - 1)
        {
            return null;
        }
        return theGrid[(int)pos.x, (int)pos.y, (int)pos.z];
    }

    public void PickPatternAndSpawnBlock()
    {
        //int randomIndex = Random.Range(0, BlocksPatterns.Count);
        BlockPattern pattern = BlocksPatterns[_nextIndex];

        //Spawn actual block
        GameObject newBlock = SpawnNewBlockByPattern(pattern);
        newBlock.AddComponent<TetrisBlock>();
        foreach (Renderer r in newBlock.GetComponentsInChildren<Renderer>())
        {
            r.material.color = pattern.Color;
        }

        //Spawn Ghost
        GameObject ghostBlock = SpawnNewBlockByPattern(pattern);
        foreach (Renderer r in ghostBlock.GetComponentsInChildren<Renderer>())
        {
            r.material.shader = Shader.Find("Transparent/Diffuse");
            Color transparentColor = pattern.Color;
            transparentColor.a = 0.15f;
            r.material.color = transparentColor;
        }
        ghostBlock.AddComponent<GhostBlock>();
        ghostBlock.GetComponent<GhostBlock>().SetParent(newBlock);

        //Set the new block for the preview window
        CalculatePreview();
        pattern = BlocksPatterns[_nextIndex];
        GameObject previewBlock = SpawnNewBlockByPattern(pattern);
        foreach (Renderer r in previewBlock.GetComponentsInChildren<Renderer>())
        {
            r.material.color = pattern.Color;
        }
        previewBlock.AddComponent<BlockRotator>();
        Previewer.instance.ShowPreview(previewBlock);
    }

    private GameObject SpawnNewBlockByPattern(BlockPattern pattern)
    {
        Vector3 spawnPosition = new Vector3((int)((int)transform.position.x + (float)GridSizeX / 2), (int)transform.position.y + GridSizeY, (int)((int)transform.position.z + (float)GridSizeZ / 2));
        GameObject blockObject = new GameObject("Block");
        blockObject.transform.position = spawnPosition;

        //Create the pivot cube in 0,0,0
        GameObject pivotCube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        pivotCube.transform.SetParent(blockObject.transform);
        pivotCube.transform.localPosition = new Vector3(0, 0, 0);

        //Spawn surrounding cubes
        foreach (Vector3 childPosition in pattern.BlocksPositions)
        {
            GameObject child = GameObject.CreatePrimitive(PrimitiveType.Cube);
            child.transform.SetParent(blockObject.transform);
            child.transform.localPosition = childPosition;
        }
        return blockObject;
    }

    private void CalculatePreview()
    {
        _nextIndex = Random.Range(0, BlocksPatterns.Count);
    }

    public void DeleteLayer()
    {
        int layersCleared = 0;
        for (int y = GridSizeY-1; y >= 0; y--)
        {
            //Check current layer
            if (IsLayerFull(y))
            {
                layersCleared++;
                //Delete blocks in layer
                DeleteBlocksAtLayer(y);

                //move all blocks by 1
                MoveAllLayerDown(y);
            }
        }
        if (layersCleared > 0)
        {
            GameManager.instance.LayerCleared(layersCleared);
        }
    }

    private void MoveAllLayerDown(int y)
    {
        for (int i = y; i < GridSizeY; i++)
        {
            MoveSingleLayerDown(i);
        }
    }

    private void MoveSingleLayerDown(int y)
    {
        for (int x = 0; x < GridSizeX; x++)
        {
            for (int z = 0; z < GridSizeZ; z++)
            {
                if (theGrid[x,y,z] != null)
                {
                    theGrid[x, y - 1, z] = theGrid[x, y, z];
                    theGrid[x, y, z] = null;
                    theGrid[x, y - 1, z].position += Vector3.down;
                }
            }
        }
    }
    
    private void DeleteBlocksAtLayer(int y)
    {
        for (int x = 0; x < GridSizeX; x++)
        {
            for (int z = 0; z < GridSizeZ; z++)
            {
                Destroy(theGrid[x, y, z].gameObject);
                theGrid[x, y, z] = null;
            }
        }
    }

    private bool IsLayerFull(int y)
    {
        for (int x = 0; x < GridSizeX; x++)
        {
            for (int z = 0; z < GridSizeZ; z++)
            {
                if (theGrid[x,y,z] == null)
                {
                    return false;
                }
            }
        }
        return true;
    }

    public Vector3 Round(Vector3 vec) 
    {
        return new Vector3(Mathf.RoundToInt(vec.x), Mathf.RoundToInt(vec.y), Mathf.RoundToInt(vec.z));
    }

    public bool CheckInsidePlayField(Vector3 pos)
    {
        return ((int)pos.x >= 0 && (int)pos.x < GridSizeX && (int)pos.z >= 0 && (int)pos.z < GridSizeZ && (int)pos.y >= 0);
    }
   
    void OnDrawGizmos()
    {
        if (bottomGrid != null) 
        {
            //Divide the size by 10 since in each unity unit we have 10 'tiles'
            Vector3 scarler = new Vector3((float)GridSizeX/10,1,(float) GridSizeZ/10);
            bottomGrid.transform.localScale = scarler;

            bottomGrid.transform.position = new Vector3(transform.position.x + (float)GridSizeX / 2, transform.position.y, transform.position.z + (float)GridSizeZ / 2);

            //Set the new number of tiles in the grid
            bottomGrid.GetComponent<MeshRenderer>().sharedMaterial.mainTextureScale = new Vector2(GridSizeX, GridSizeZ);
        }

        if (North != null)
        {
            //Divide the size by 10 since in each unity unit we have 10 'tiles'
            Vector3 scarler = new Vector3((float)GridSizeX / 10, 1, (float)GridSizeY / 10);
            North.transform.localScale = scarler;

            North.transform.position = new Vector3(transform.position.x + (float)GridSizeX / 2, transform.position.y + (float)GridSizeY / 2, transform.position.z + (float)GridSizeZ);

            //Set the new number of tiles in the grid
            North.GetComponent<MeshRenderer>().sharedMaterial.mainTextureScale = new Vector2(GridSizeX, GridSizeY);
        }

        if (South != null)
        {
            //Divide the size by 10 since in each unity unit we have 10 'tiles'
            Vector3 scarler = new Vector3((float)GridSizeX / 10, 1, (float)GridSizeY / 10);
            South.transform.localScale = scarler;

            South.transform.position = new Vector3(transform.position.x + (float)GridSizeX / 2, transform.position.y + (float)GridSizeY / 2, transform.position.z);
        }

        if (East != null)
        {
            //Divide the size by 10 since in each unity unit we have 10 'tiles'
            Vector3 scarler = new Vector3((float)GridSizeZ / 10, 1, (float)GridSizeY / 10);
            East.transform.localScale = scarler;

            East.transform.position = new Vector3(transform.position.x + GridSizeX, transform.position.y + (float)GridSizeY / 2, transform.position.z + (float)GridSizeZ / 2);

            //Set the new number of tiles in the grid
            East.GetComponent<MeshRenderer>().sharedMaterial.mainTextureScale = new Vector2(GridSizeZ, GridSizeY);
        }

        if (West != null)
        {
            //Divide the size by 10 since in each unity unit we have 10 'tiles'
            Vector3 scarler = new Vector3((float)GridSizeZ / 10, 1, (float)GridSizeY / 10);
            West.transform.localScale = scarler;

            West.transform.position = new Vector3(transform.position.x, transform.position.y + (float)GridSizeY / 2, transform.position.z + (float)GridSizeZ / 2);
        }
    }
}
